# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = "letsbit-kit"
  spec.version = `git describe --tags`.chomp.gsub(/^v/, "")
  spec.authors = ["lisandromaselli@letsbit.io"]

  spec.summary = "Instrumentation for Letsbit"
  spec.license = "MIT"

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec|tools)/}) }
  spec.require_paths = ["lib"]
  spec.required_ruby_version = ">= 3.1.3"

  # Please maintain alphabetical order for dependencies
  spec.add_runtime_dependency "actionpack", ">= 5.0.0", "< 8.0.0"
  spec.add_runtime_dependency "activesupport", ">= 5.0.0", "< 8.0.0"

  # Please maintain alphabetical order for dev dependencies
  spec.add_development_dependency "pry", "~> 0.12"
  spec.add_development_dependency "rack", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.3"
  spec.add_development_dependency "rest-client", "~> 2.1.0"
  spec.add_development_dependency "rspec", "~> 3.12.0"
  spec.add_development_dependency "rspec-parameterized", "~> 1.0"
  spec.add_development_dependency "rubocop"
end
