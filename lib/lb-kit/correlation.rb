# frozen_string_literal: true

module LBKit
  # Correlation provides correlation functionality
  module Correlation
    autoload :GRPC, 'lb-kit/correlation/grpc'

    autoload :CorrelationId, 'lb-kit/correlation/correlation_id'
  end
end
