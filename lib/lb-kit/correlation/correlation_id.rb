# frozen_string_literal: true

module LBKit
  module Correlation
    # CorrelationId module provides access the Correlation-ID
    # of the current request
    module CorrelationId
      LOG_KEY = LBKit::Context::CORRELATION_ID_KEY

      class << self
        def use_id(correlation_id)
          LBKit::Context.with_context(LOG_KEY => correlation_id) do |context|
            yield(context.correlation_id)
          end
        end

        def current_id
          LBKit::Context.correlation_id
        end

        def current_or_new_id
          current_id || LBKit::Context.push.correlation_id
        end
      end
    end
  end
end
