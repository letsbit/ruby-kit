# frozen_string_literal: true

module LBKit
  # Logging provides functionality for logging, such as
  # sanitization
  module Logging
    autoload :Sanitizer, 'lb-kit/logging/sanitizer'
    autoload :Formatter, 'lb-kit/logging/formatter'
    autoload :JsonLogger, 'lb-kit/logging/json_logger'
  end
end
