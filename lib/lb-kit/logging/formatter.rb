# frozen_string_literal: true

require 'time'
require 'logger'
require 'json'

module LBKit
  module Logging
    class Formatter < ::Logger::Formatter
      RESERVED_LOG_KEYS = %i[
        _id
        _index
        _score
        _type
      ].freeze

      def call(severity, timestamp, progname, msg)
        data = default_attributes
        data['log.level'] = severity
        data['@timestamp'] = timestamp.utc.iso8601(3)
        data['ecs.version'] = '1.4.0'
        data['log.logger'] = progname if progname

        case msg
        when String
          data[:message] = msg
        when Hash
          reject_reserved_log_keys!(msg)
          data.merge!(msg)
        end

        dump_json(data) << "\n"
      end

      private

      def default_attributes
        {}
      end

      def dump_json(data)
        JSON.fast_generate(data)
      end

      def reject_reserved_log_keys!(hash)
        return if ENV['RAILS_ENV'] == 'production'

        reserved_keys_used = hash.transform_keys(&:to_sym).slice(*RESERVED_LOG_KEYS)
        return unless reserved_keys_used.any?

        raise "The following log keys used are reserved: #{reserved_keys_used.keys.join(', ')}" \
              "\n\nUse key names that are descriptive e.g. by using a prefix."
      end
    end
  end
end
