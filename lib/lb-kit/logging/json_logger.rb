# frozen_string_literal: true

require 'time'
require 'logger'
require 'json'

module LBKit
  module Logging
    class JsonLogger < ::Logger
      RESERVED_LOG_KEYS = %i[
        _id
        _index
        _score
        _type
      ].freeze

      def self.log_level(fallback: ::Logger::DEBUG)
        ENV.fetch('LOG_LEVEL', fallback)
      end

      def self.exclude_context!
        @exclude_context = true
        self
      end

      def self.exclude_context?
        !!@exclude_context
      end

      def initialize(path, level: JsonLogger.log_level)
        super
        self.formatter = Formatter.new
      end

      def format_message(severity, timestamp, progname, msg, **extras)
        data = {}

        if self.class.exclude_context?
          data[LBKit::Correlation::CorrelationId::LOG_KEY] = LBKit::Correlation::CorrelationId.current_id
        else
          data.merge!(LBKit::Context.current.to_h)
        end

        case msg
        when String
          data[:message] = msg
        when Hash
          data.merge!(msg)
        end

        data.merge!(extras) if extras

        formatter.call(severity, timestamp, progname, data)
      end

      private

      def dump_json(data)
        JSON.fast_generate(data)
      end
    end
  end
end
