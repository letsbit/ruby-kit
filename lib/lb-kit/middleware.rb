# frozen_string_literal: true

module LBKit
  # Adds middlewares for using in rack
  module Middleware
    autoload :Rack, 'lb-kit/middleware/rack'
  end
end
