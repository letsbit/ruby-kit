# rubocop:disable Naming/FileName
# frozen_string_literal: true

# LBKit is a module for handling cross-project
# infrastructural concerns, partcularly related to
# observability.
module LBKit
  autoload :Correlation, 'lb-kit/correlation'
  autoload :Context, 'lb-kit/context'
  autoload :Logging, 'lb-kit/logging'
  autoload :Middleware, 'lb-kit/middleware'
end

# rubocop:enable Naming/FileName
