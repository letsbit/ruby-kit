# frozen_string_literal: true

RSpec.describe LBKit::Logging::JsonLogger do
  include StubENV

  subject { described_class.new('/dev/null') }

  let(:now) { Time.now }

  around do |example|
    LBKit::Context.with_context { example.run }
  end

  describe '.initialize' do
    it 'builds logger using log_level' do
      expect(described_class).to receive(:log_level).and_return(:warn)

      expect(subject.level).to eq(described_class::WARN)
    end

    it 'raises ArgumentError if invalid log level' do
      allow(described_class).to receive(:log_level).and_return(:invalid)

      expect { subject.level }.to raise_error(ArgumentError, 'invalid log level: invalid')
    end

    using RSpec::Parameterized::TableSyntax

    where(:env_value, :resulting_level) do
      0 | described_class::DEBUG
      :debug | described_class::DEBUG
      'debug' | described_class::DEBUG
      'DEBUG' | described_class::DEBUG
      'DeBuG' | described_class::DEBUG
      1 | described_class::INFO
      :info | described_class::INFO
      'info' | described_class::INFO
      'INFO' | described_class::INFO
      'InFo' | described_class::INFO
      2 | described_class::WARN
      :warn | described_class::WARN
      'warn' | described_class::WARN
      'WARN' | described_class::WARN
      'WaRn' | described_class::WARN
      3 | described_class::ERROR
      :error | described_class::ERROR
      'error' | described_class::ERROR
      'ERROR' | described_class::ERROR
      'ErRoR' | described_class::ERROR
      4 | described_class::FATAL
      :fatal | described_class::FATAL
      'fatal' | described_class::FATAL
      'FATAL' | described_class::FATAL
      'FaTaL' | described_class::FATAL
      5 | described_class::UNKNOWN
      :unknown | described_class::UNKNOWN
      'unknown' | described_class::UNKNOWN
      'UNKNOWN' | described_class::UNKNOWN
      'UnKnOwN' | described_class::UNKNOWN
    end

    with_them do
      it 'builds logger if valid log level' do
        stub_env('LOG_LEVEL', env_value)

        expect(subject.level).to eq(resulting_level)
      end
    end
  end

  describe '.log_level' do
    context 'if LOG_LEVEL is set' do
      before do
        stub_env('LOG_LEVEL', described_class::ERROR)
      end

      it 'returns value of LOG_LEVEL' do
        expect(described_class.log_level).to eq(described_class::ERROR)
      end

      it 'ignores fallback' do
        expect(described_class.log_level(fallback: described_class::FATAL)).to eq(described_class::ERROR)
      end
    end

    context 'if LOG_LEVEL is not set' do
      it 'returns default fallback DEBUG' do
        expect(described_class.log_level).to eq(described_class::DEBUG)
      end

      it 'returns passed fallback' do
        expect(described_class.log_level(fallback: described_class::FATAL)).to eq(described_class::FATAL)
      end
    end
  end

  it 'appends newline' do
    output = subject.format_message('INFO', now, 'test', 'Hello world')

    expect(output).to end_with("\n")
  end

  it 'formats strings' do
    output = subject.format_message('INFO', now, 'test', 'Hello world')
    data = JSON.parse(output)

    expect(data['log.level']).to eq('INFO')
    expect(data['@timestamp']).to eq(now.utc.iso8601(3))
    expect(data['message']).to eq('Hello world')
    expect(data['correlation_id']).to be_present
  end

  it 'formats hashes' do
    output = subject.format_message('INFO', now, 'test', {hello: 1})
    data = JSON.parse(output)

    expect(data['log.level']).to eq('INFO')
    expect(data['@timestamp']).to eq(now.utc.iso8601(3))
    expect(data['hello']).to eq(1)
    expect(data['message']).to be_nil
    expect(data['correlation_id']).to be_present
  end

  it 'uses indifferent log key access' do
    output = subject.format_message('INFO', now, 'test', {hello: 1, "world": 2})
    data = JSON.parse(output)

    expect(data['hello']).to eq(1)
    expect(data['world']).to eq(2)
  end

  it 'includes the logging context in the message' do
    LBKit::Context.with_context('hello' => 'world') do
      output = subject.format_message('INFO', now, 'test', 'Log message')
      data = JSON.parse(output)

      expect(data).to include(
        'correlation_id' => LBKit::Correlation::CorrelationId.current_id, "#{LBKit::Context::LOG_KEY}.hello" => 'world'
      )
    end
  end

  it 'does not override logging context passed directly into the message' do
    LBKit::Context.with_context('hello' => 'world') do
      output = subject.format_message('INFO', now, 'test', {"#{LBKit::Context::LOG_KEY}.hello" => 'brave world'})
      data = JSON.parse(output)

      expect(data).to include(
        'correlation_id' => LBKit::Correlation::CorrelationId.current_id, "#{LBKit::Context::LOG_KEY}.hello" => 'brave world'
      )
    end
  end

  context 'when exclude_context is set' do
    let(:logger) do
      Class.new(described_class) do
        exclude_context!
      end
    end

    subject { logger.new('/dev/null') }

    it 'does not includes the logging context in the message' do
      LBKit::Context.with_context('hello' => 'world') do
        output = subject.format_message('INFO', now, 'test', 'Log message')
        data = JSON.parse(output)

        expect(data).to include(
          'correlation_id' => LBKit::Correlation::CorrelationId.current_id
        )

        expect(data).not_to include(
          "#{LBKit::Context::LOG_KEY}.hello" => 'world'
        )
      end
    end
  end

  describe 'reserved log keys' do
    let(:reserved_key_data) { described_class::RESERVED_LOG_KEYS.to_h {|k| [k, 42] } }
    let(:expected_error) {
      /^The following log keys used are reserved: #{described_class::RESERVED_LOG_KEYS.join(', ')}.*/
    }

    shared_examples 'raise error' do
      it 'raises an error' do
        expect { subject.format_message('INFO', now, 'test', reserved_key_data) }.to raise_error(expected_error)
      end

      describe 'symbol and string indifference' do
        it 'raises error when reserved key is a symbol' do
          expect {
            subject.format_message('INFO', now, 'test',
                                   reserved_key_data.transform_keys(&:to_sym))
          }.to raise_error(expected_error)
        end

        it 'raises error when reserved key is a string' do
          expect {
            subject.format_message('INFO', now, 'test', reserved_key_data.transform_keys(&:to_s))
          }.to raise_error(expected_error)
        end
      end
    end

    describe 'in test env' do
      it_behaves_like 'raise error'
    end

    describe 'in dev env' do
      before do
        stub_env('RAILS_ENV', 'development')
      end

      it_behaves_like 'raise error'
    end

    describe 'in production env' do
      before do
        stub_env('RAILS_ENV', 'production')
      end

      it 'does not raise an error' do
        expect { subject.format_message('INFO', now, 'test', reserved_key_data) }.not_to raise_error
      end
    end
  end
end
