# frozen_string_literal: true

require 'letsbit-kit'
require 'rspec-parameterized'

require_relative 'helpers/stub_env'

# Warning#[]= is supported since Ruby 2.7
Warning[:deprecated] = true if Warning.respond_to?(:[]=)

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  Kernel.srand config.seed
end
